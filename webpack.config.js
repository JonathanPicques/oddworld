const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    "entry": "./src/index.ts",
    "devtool": "inline-source-map",
    "resolve": {
        "extensions": [".js", ".ts"]
    },
    "module": {
        "rules": [
            {
                "test": /\.tsx?$/,
                "use": "ts-loader",
                "exclude": /node_modules/
            }
        ]
    },
    "output": {
        "filename": "[name].js",
        "path": path.resolve(__dirname, "dist")
    },
    "devServer": {
        "open": true,
        "filename": path.resolve(__dirname, "dist")
    },
    "plugins": [
        new HtmlWebpackPlugin({
            "template": "index.ejs",
            "title": "Oddworld Engine"
        }),
        new CopyWebpackPlugin([
            {"from": path.resolve(__dirname, "assets"), "to": "assets"}
        ])
    ]
};
