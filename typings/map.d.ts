interface Map<K, V> {
    has(key: K): boolean;
    get(key: K): V;
    set(key: K, value?: V): Map<K, V>;
    delete(key: K): boolean;
    clear(): void;
    size: number;
}

interface MapConstructor {
    new <K, V>(): Map<K, V>;
    prototype: Map<any, any>;
}
declare var Map: MapConstructor;