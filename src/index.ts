import {Game} from "./game/game";
import {RuptureFarmLevel} from "./game/levels/rupturefarm/rupturefarm";

(async () => {
    const game = new Game();
    const level = new RuptureFarmLevel(game);

    await game.start();
    await game.changeLevel(level);
})();