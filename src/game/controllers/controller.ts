import {IPawn} from "./pawn";

/**
 * @name IController
 */
export interface IController {

    /**
     * @name possess
     * @desc take possession of the given pawn
     * @param {IPawn} pawn
     */
    possess(pawn: IPawn): void;

    /**
     * @name unpossess
     * @desc release possession of the given pawn
     */
    unpossess(): void;

}