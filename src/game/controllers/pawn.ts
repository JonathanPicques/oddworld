/**
 * @name PawnEntity
 */
import {IController} from "./controller";

/**
 * @name IPawn
 */
export interface IPawn {

    /**
     * @name controller
     * @desc controller possessing this pawn
     */
    controller: IController;

}