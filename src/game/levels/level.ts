import {Scene, GridHelper} from "three";

import {Game} from "../game";
import {Entity} from "../entities/entity";
import {ICoroutine} from "../utils/coroutine/coroutine";

/**
 * @name Level
 */
export abstract class Level {

    /**
     * @name game
     * @desc reference to the game
     */
    public readonly game: Game;
    /**
     * @name scene
     * @desc the threejs scene to render this level
     */
    public readonly scene: Scene;
    /**
     * @name entities
     * @desc the level entities
     */
    public readonly entities: Array<Entity>;

    /**
     * @name entitiesAdd
     * @desc implementation: entities buffer to be added on the next tick
     */
    private entitiesAdd: Array<Entity>;
    /**
     * @name entitiesRemove
     * @desc implementation: entities buffer to be removed on the next tick
     */
    private entitiesRemove: Array<Entity>;

    /**
     * @name constructor
     * @param {Game} game
     */
    public constructor(game: Game) {
        this.game = game;
        this.scene = new Scene();
        this.entities = [];
        this.entitiesAdd = [];
        this.entitiesRemove = [];

        this.scene.add(new GridHelper(35 * 100, 100));
        this.scene.children[0].rotation.x = Math.PI / 2;
    }

    /**
     * @name start
     * @desc start the level and load important assets
     * @returns {ICoroutine}
     */
    public * start(): ICoroutine {

    }

    /**
     * @name update
     * @desc tick the entities
     */
    public update(): void {
        for (let i = this.entitiesRemove.length - 1; i >= 0; i--) {
            this.entitiesRemove[i].stop();
            this.entities.splice(i, 1);
        }
        for (let i = this.entitiesAdd.length - 1; i >= 0; i--) {
            const entity = this.entitiesAdd[i];
            entity.start();
            this.entities.unshift(this.entitiesAdd[i]);
        }
        this.entitiesAdd = [];
        this.entitiesRemove = [];
        for (let i = this.entities.length - 1; i >= 0; i--) {
            this.entities[i].update();
        }
    }

    /**
     * @name stop
     * @desc stop the level, unload assets and stop entities
     * @returns {ICoroutine}
     */
    public * stop(): ICoroutine {
        for (let i = this.entities.length - 1; i >= 0; i--) {
            this.entities[i].stop();
        }
    }

    /**
     * @name addEntity
     * @desc add the given entity to the level on the next tick
     * @param {Entity} entity
     */
    public addEntity(entity: Entity): void {
        if (entity.level !== this) {
            throw new Error("Level::addEntity() called with an entity from a different level");
        }
        if (this.entitiesRemove.indexOf(entity) !== -1) {
            throw new Error("Level::addEntity() called with an entity going to be removed");
        }
        if (this.entities.indexOf(entity) === -1 && this.entitiesAdd.indexOf(entity) === -1) {
            this.entitiesAdd.push(entity);
        }
    }

    /**
     * @name removeEntity
     * @desc remove the given entity from the level on the next tick
     * @param {Entity} entity
     */
    public removeEntity(entity: Entity): void {
        if (entity.level !== this) {
            throw new Error("Level::removeEntity() called with an entity from a different level");
        }
        if (this.entitiesAdd.indexOf(entity) !== -1) {
            throw new Error("Level::removeEntity() called with an entity going to be added");
        }
        if (this.entities.indexOf(entity) !== -1 && this.entitiesRemove.indexOf(entity) !== -1) {
            this.entitiesRemove.push(entity);
        }
    }

}