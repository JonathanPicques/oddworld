import {Level} from "../level";
import {ICoroutine} from "../../utils/coroutine/coroutine";
import {MudokonEntity} from "../../entities/alive/mudokon/mudokon";
import {PlayerController} from "../../entities/player/player";

/**
 * @name RuptureFarmLevel
 */
export class RuptureFarmLevel extends Level {

    /**
     * @name start
     * @desc {@link Level.start}
     * @returns {ICoroutine}
     */
    public * start(): ICoroutine {
        yield* MudokonEntity.LoadAssets(this.game.assetLoader);

        const mudokon = new MudokonEntity(this);
        const playerController = new PlayerController(this);

        playerController.possess(mudokon);

        this.addEntity(playerController);
        this.addEntity(mudokon);
    }

}