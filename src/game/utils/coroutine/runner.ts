import {ICoroutine} from "./coroutine";

/**
 * @name CoroutineRunner
 */
export class CoroutineRunner {

    /**
     * @name coroutines
     * @desc coroutines running
     */
    private readonly coroutines: Array<ICoroutine>;

    /**
     * @name coroutinesStart
     * @desc implementation: coroutines buffer to be started on the next iteration
     */
    private coroutinesStart: Array<ICoroutine>;
    /**
     * @name coroutinesStop
     * @desc implementation: coroutines buffer to be stopped on the next iteration
     */
    private coroutinesStop: Array<ICoroutine>;

    /**
     * @name constructor
     */
    public constructor() {
        this.coroutines = [];
        this.coroutinesStart = [];
        this.coroutinesStop = [];
    }

    /**
     * @name startCoroutine
     * @desc starts the given coroutine on the next iteration
     * @param {ICoroutine} coroutine
     */
    public startCoroutine(coroutine: ICoroutine): void {
        if (this.coroutinesStop.indexOf(coroutine) !== -1) {
            throw new Error("CoroutineRunner::startCoroutine() called with a coroutine going to be stopped");
        }
        if (this.coroutines.indexOf(coroutine) === -1 && this.coroutinesStart.indexOf(coroutine) === -1) {
            this.coroutinesStart.push(coroutine);
        }
    }

    /**
     * @name stopCoroutine
     * @desc stops the given coroutine
     * @param {ICoroutine} coroutine
     */
    public stopCoroutine(coroutine: ICoroutine): void {
        if (this.coroutinesStart.indexOf(coroutine) !== -1) {
            throw new Error("CoroutineRunner::stopCoroutine() called with an coroutine going to be started");
        }
        if (this.coroutines.indexOf(coroutine) !== -1 && this.coroutinesStop.indexOf(coroutine) !== -1) {
            this.coroutinesStop.push(coroutine);
        }
    }

    /**
     * @name clear
     * @desc stops all coroutines
     */
    public clear(): void {
        this.coroutinesStart = [];
        this.coroutinesStop = [...this.coroutines];
    }

    /**
     * @name runCoroutines
     * @desc run one iteration of the active coroutines
     */
    public runCoroutines(): void {
        for (let i = this.coroutinesStop.length - 1; i >= 0; i--) {
            const coroutine = this.coroutinesStop[i];
            coroutine.interrupted = true;
            coroutine.end && coroutine.end(coroutine);
            this.coroutines.splice(i, 1);
        }
        for (let i = this.coroutinesStart.length - 1; i >= 0; i--) {
            this.coroutines.unshift(this.coroutinesStart[i]);
        }
        this.coroutinesStart = [];
        this.coroutinesStop = [];
        for (let i = this.coroutines.length - 1; i >= 0; i--) {
            const coroutine = this.coroutines[i];
            const nextYield = coroutine.next(coroutine.value);
            coroutine.value = nextYield.value;
            if (nextYield.done) {
                this.coroutines.splice(i, 1);
                coroutine.done = true;
                coroutine.end && coroutine.end(coroutine);
            } else {
                coroutine.done = false;
                coroutine.update && coroutine.update(coroutine);
            }
        }
    }

}