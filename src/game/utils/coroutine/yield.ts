import {ICoroutine} from "./coroutine";

/**
 * @name yieldForSeconds
 * @desc return a coroutine to wait for the given number of seconds
 * @param {number} seconds
 * @param {Function} scale
 * @returns {ICoroutine}
 */
export function* yieldForScaledSeconds(seconds: number, scale: () => number): ICoroutine<number> {
    for (let i = 0; i < seconds; i += scale()) {
        yield i;
    }
    return seconds;
}

/**
 * @name yieldForPromise
 * @desc return a coroutine to wait for the promise to resolve
 * @param {Promise} promise
 * @returns {ICoroutine}
 */
export function* yieldForPromise<T>(promise: Promise<T>): ICoroutine<T> {
    let resolved = false;
    let value = undefined;
    promise.then(v => {
        resolved = true;
        value = v;
        return v;
    });
    while (!resolved) {
        yield;
    }
    return value;
}