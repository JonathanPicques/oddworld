/**
 * @name ICoroutine
 * @desc coroutines are generator functions that yields every iteration
 */
export interface ICoroutine<T = any> extends IterableIterator<T> {
    /**
     * @name value
     * @desc last yielded value of the coroutine
     */
    value?: any;

    /**
     * @name done
     * @desc true when the coroutine gracefully ended
     */
    done?: boolean;
    /**
     * @name interrupted
     * @desc true when the coroutine was abruptly stopped
     */
    interrupted?: boolean;

    /**
     * @name end
     * @desc called when the coroutine gracefully ended or was abruptly stopped
     * @callback
     */
    end?: (coroutine: this) => void;
    /**
     * @name update
     * @desc called after each yield
     * @callback
     */
    update?: (coroutine: this) => void;
}