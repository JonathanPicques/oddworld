/**
 * @name InputGamepadAxis
 */
export enum InputGamepadAxis {

    LEFT_X_AXIS = 0,
    LEFT_Y_AXIS = 1,
    RIGHT_X_AXIS = 2,
    RIGHT_Y_AXIS = 3

}

/**
 * @name InputGamepadButton
 */
export enum InputGamepadButton {

    A = 0,
    B = 1,
    X = 2,
    Y = 3,

    L_BUMPER = 4,
    R_BUMPER = 5,

    L_TRIGGER = 6,
    R_TRIGGER = 7,

    BACK = 8,
    START = 9,

    L_CLICK = 10,
    R_CLICK = 11,

    DPAD_UP = 12,
    DPAD_DOWN = 13,
    DPAD_LEFT = 14,
    DPAD_RIGHT = 15

}

/**
 * @name InputGamepadMappings
 * @desc mapped gamepads
 */
export const InputGamepadMappings: { [key: string]: InputGamepadMapping } = {
    "Xbox 360 Controller": {
        "buttons": {
            "0": InputGamepadButton.A,
            "1": InputGamepadButton.B,
            "2": InputGamepadButton.X,
            "3": InputGamepadButton.Y,

            "4": InputGamepadButton.L_BUMPER,
            "5": InputGamepadButton.R_BUMPER,

            "6": InputGamepadButton.L_TRIGGER,
            "7": InputGamepadButton.R_TRIGGER,

            "8": InputGamepadButton.BACK,
            "9": InputGamepadButton.START,

            "10": InputGamepadButton.L_CLICK,
            "11": InputGamepadButton.R_CLICK,

            "12": InputGamepadButton.DPAD_UP,
            "13": InputGamepadButton.DPAD_DOWN,
            "14": InputGamepadButton.DPAD_LEFT,
            "15": InputGamepadButton.DPAD_RIGHT
        },
        "axes": {
            "0": {
                "axis": InputGamepadAxis.LEFT_X_AXIS,
                "deadzone": 0.1
            },
            "1": {
                "axis": InputGamepadAxis.LEFT_Y_AXIS,
                "deadzone": 0.1
            },
            "2": {
                "axis": InputGamepadAxis.RIGHT_X_AXIS,
                "deadzone": 0.1
            },
            "3": {
                "axis": InputGamepadAxis.RIGHT_Y_AXIS,
                "deadzone": 0.1
            }
        }
    }
};

/**
 * @name InputGamepadMapping
 */
export type InputGamepadMapping = { "buttons": { [key: string]: InputGamepadButton }, "axes": { [key: string]: { "axis": InputGamepadAxis, "deadzone": number } } };

/**
 * @name SelectInputGamepadMapping
 * @desc return the best suited mapping from the given gamepad
 * @param {Gamepad} gamepad
 * @returns {InputGamepadMapping}
 */
export function SelectInputGamepadMapping(gamepad: Gamepad): InputGamepadMapping {
    if (gamepad.id.includes("Xbox")) {
        return InputGamepadMappings["Xbox 360 Controller"];
    }
    throw new Error("SelectMapping() gamepad not mapped");
}