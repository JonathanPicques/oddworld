/**
 * @name InputMouseButton
 */
export enum InputMouseButton {

    MOUSE_LEFT = 0,
    MOUSE_MIDDLE = 1,
    MOUSE_RIGHT = 2,

}