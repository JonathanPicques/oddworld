import {InputMouseButton} from "./mouse";
import {InputKeyboardKey} from "./keyboard";
import {InputGamepadAxis, InputGamepadButton} from "./gamepad";

/**
 * @name InputManager
 */
export class InputManager {

    /**
     * @name element
     * @desc element used for input event listeners
     */
    private readonly element: HTMLElement;

    /**
     * @name gamepads
     * @desc map of plugged gamepads and their axes/buttons states
     */
    private readonly gamepads: Map<number, { "buttons": Map<InputGamepadButton, number>, "axes": Map<InputGamepadAxis, number> }>;
    /**
     * @name keyboardKeys
     * @desc map of held keyboard keys and timestamp
     */
    private readonly keyboardKeys: Map<InputKeyboardKey, number>;
    /**
     * @name mouseButtons
     * @desc map of held mouse keys and timestamp
     */
    private readonly mouseButtons: Map<InputMouseButton, number>;

    /**
     * @name mousePosition
     * @desc mouse position in the canvas
     */
    public readonly mousePosition: [number, number];

    /**
     * @name constructor
     * @param {HTMLElement} element
     */
    public constructor(element: HTMLElement) {
        this.element = element;

        this.gamepads = new Map();
        this.mouseButtons = new Map();
        this.keyboardKeys = new Map();
        this.mousePosition = [0, 0];

        this.initializeElementListeners();
    }

    /**
     * @name initializeElementListeners
     */
    private initializeElementListeners() {
        this.element.tabIndex = 1;
        this.element.addEventListener("mousemove", e => {
            this.mousePosition[0] = e.clientX;
            this.mousePosition[1] = e.clientY;
        });
        this.element.addEventListener("pointermove", e => {
            this.mousePosition[0] = e.clientX;
            this.mousePosition[1] = e.clientY;
        });
        this.element.addEventListener("pointerdown", e => {
            switch (e.pointerType) {
                case "mouse":
                    this.mouseButtons.set(InputMouseButton.MOUSE_LEFT + e.button, new Date().getTime());
                    break;
                case "touch":
                    this.mouseButtons.set(InputMouseButton.MOUSE_LEFT, new Date().getTime());
                    break;
            }
        });
        this.element.addEventListener("pointerup", e => {
            switch (e.pointerType) {
                case "mouse":
                    this.mouseButtons.delete(InputMouseButton.MOUSE_LEFT + e.button);
                    break;
                case "touch":
                    this.mouseButtons.delete(InputMouseButton.MOUSE_LEFT);
                    break;
            }
        });
        this.element.addEventListener("keydown", e => {
            this.keyboardKeys.set(e.keyCode, new Date().getTime());
        });
        this.element.addEventListener("keyup", e => {
            this.keyboardKeys.delete(e.keyCode);
        });
        window.addEventListener("blur", () => {
            this.keyboardKeys.clear();
        });
    }

    /**
     * @name gamepadAxis
     * @desc return the given gamepad axis value
     * @param {number} gamepadIndex
     * @param {InputGamepadAxis} gamepadAxis
     * @returns {number}
     */
    gamepadAxis(gamepadIndex: number, gamepadAxis: InputGamepadAxis): number {
        const gamepad = this.gamepads.get(gamepadIndex);
        if (typeof gamepad !== "undefined") {
            return gamepad.axes.get(gamepadAxis);
        }
        return 0;
    }

    /**
     * @name gamepadButtonDown
     * @desc return whether the given gamepad button is down (held)
     * @param {number} gamepadIndex
     * @param {InputGamepadButton} gamepadButton
     * @returns {boolean}
     */
    gamepadButtonDown(gamepadIndex: number, gamepadButton: InputGamepadButton): boolean {
        const gamepad = this.gamepads.get(gamepadIndex);
        if (typeof gamepad !== "undefined") {
            return gamepad.buttons.has(gamepadButton);
        }
        return false;
    }

    /**
     * @name gamepadButtonDown
     * @desc return whether the given gamepad button is up (not held)
     * @param {number} gamepadIndex
     * @param {InputGamepadButton} gamepadButton
     * @returns {boolean}
     */
    gamepadButtonUp(gamepadIndex: number, gamepadButton: InputGamepadButton): boolean {
        const gamepad = this.gamepads.get(gamepadIndex);
        if (typeof gamepad !== "undefined") {
            return !gamepad.buttons.has(gamepadButton);
        }
        return false;
    }

    /**
     * @name keyboardKeyDown
     * @desc return whether the given keyboard key is down (held)
     * @param {InputKeyboardKey} keyboardKey
     * @returns {boolean}
     */
    keyboardKeyDown(keyboardKey: InputKeyboardKey): boolean {
        return this.keyboardKeys.has(keyboardKey);
    }

    /**
     * @name keyboardKeyUp
     * @desc return whether the given keyboard key is up (not held)
     * @param {InputKeyboardKey} keyboardKey
     * @returns {boolean}
     */
    keyboardKeyUp(keyboardKey: InputKeyboardKey): boolean {
        return !this.keyboardKeys.has(keyboardKey);
    }

    /**
     * @name mouseButtonDown
     * @desc return whether the given mouse button is down (held)
     * @param {InputMouseButton} mouseButton
     * @returns {boolean}
     */
    mouseButtonDown(mouseButton: InputMouseButton): boolean {
        return this.mouseButtons.has(mouseButton);
    }

    /**
     * @name mouseButtonUp
     * @desc return whether the given mouse button is up (not held)
     * @param {InputMouseButton} mouseButton
     * @returns {boolean}
     */
    mouseButtonUp(mouseButton: InputMouseButton): boolean {
        return !this.mouseButtons.has(mouseButton);
    }

}