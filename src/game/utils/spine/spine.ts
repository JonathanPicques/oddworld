import spine from "../../../vendors/spine/spine-threejs";

/**
 * @name setAnimationOnce
 * @desc set the animation if different
 * @param {spine.threejs.SkeletonMesh} skeletonMesh
 * @param {string} animationName
 * @param {boolean} loop
 */
export function setAnimationOnce(skeletonMesh: spine.threejs.SkeletonMesh, animationName: string, loop: boolean): void {
    if (skeletonMesh.state.getCurrent(0).animation.name !== animationName) {
        skeletonMesh.state.setAnimation(0, animationName, loop);
    }
}