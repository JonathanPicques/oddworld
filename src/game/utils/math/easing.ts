/**
 * @name TEasing
 * @desc easing function type
 */
export type TEasing = (t: number) => number;

/**
 * @name linear
 * @desc linear
 * @param {number} t
 * @returns {number}
 */
export function linear(t: number) {
    return t;
}

/**
 * @name inQuad
 * @desc inQuad
 * @param {number} t
 * @returns {number}
 */
export function inQuad(t: number) {
    return t * t;
}

/**
 * @name outQuad
 * @desc outQuad
 * @param {number} t
 * @returns {number}
 */
export function outQuad(t: number) {
    return t * (2 - t);
}

/**
 * @name inOutQuad
 * @desc inOutQuad
 * @param {number} t
 * @returns {number}
 */
export function inOutQuad(t: number) {
    t *= 2;
    if (t < 1) return 0.5 * t * t;
    return -0.5 * (--t * (t - 2) - 1);
}

/**
 * @name inCube
 * @desc inCube
 * @param {number} t
 * @returns {number}
 */
export function inCube(t: number) {
    return t * t * t;
}

/**
 * @name outCube
 * @desc outCube
 * @param {number} t
 * @returns {number}
 */
export function outCube(t: number) {
    return --t * t * t + 1;
}

/**
 * @name inOutCube
 * @desc inOutCube
 * @param {number} t
 * @returns {number}
 */
export function inOutCube(t: number) {
    t *= 2;
    if (t < 1) return 0.5 * t * t * t;
    return 0.5 * ((t -= 2) * t * t + 2);
}

/**
 * @name inQuart
 * @desc inQuart
 * @param {number} t
 * @returns {number}
 */
export function inQuart(t: number) {
    return t * t * t * t;
}

/**
 * @name outQuart
 * @desc outQuart
 * @param {number} t
 * @returns {number}
 */
export function outQuart(t: number) {
    return 1 - (--t * t * t * t);
}

/**
 * @name inOutQuart
 * @desc inOutQuart
 * @param {number} t
 * @returns {number}
 */
export function inOutQuart(t: number) {
    t *= 2;
    if (t < 1) return 0.5 * t * t * t * t;
    return -0.5 * ((t -= 2) * t * t * t - 2);
}

/**
 * @name inQuint
 * @desc inQuint
 * @param {number} t
 * @returns {number}
 */
export function inQuint(t: number) {
    return t * t * t * t * t;
}

/**
 * @name outQuint
 * @desc outQuint
 * @param {number} t
 * @returns {number}
 */
export function outQuint(t: number) {
    return --t * t * t * t * t + 1;
}

/**
 * @name inOutQuint
 * @desc inOutQuint
 * @param {number} t
 * @returns {number}
 */
export function inOutQuint(t: number) {
    t *= 2;
    if (t < 1) return 0.5 * t * t * t * t * t;
    return 0.5 * ((t -= 2) * t * t * t * t + 2);
}

/**
 * @name inSine
 * @desc inSine
 * @param {number} t
 * @returns {number}
 */
export function inSine(t: number) {
    return 1 - Math.cos(t * Math.PI / 2);
}

/**
 * @name outSine
 * @desc outSine
 * @param {number} t
 * @returns {number}
 */
export function outSine(t: number) {
    return Math.sin(t * Math.PI / 2);
}

/**
 * @name inOutSine
 * @desc inOutSine
 * @param {number} t
 * @returns {number}
 */
export function inOutSine(t: number) {
    return .5 * (1 - Math.cos(Math.PI * t));
}

/**
 * @name inExpo
 * @desc inExpo
 * @param {number} t
 * @returns {number}
 */
export function inExpo(t: number) {
    return 0 === t ? 0 : Math.pow(1024, t - 1);
}

/**
 * @name outExpo
 * @desc outExpo
 * @param {number} t
 * @returns {number}
 */
export function outExpo(t: number) {
    return 1 === t ? t : 1 - Math.pow(2, -10 * t);
}

/**
 * @name inOutExpo
 * @desc inOutExpo
 * @param {number} t
 * @returns {number}
 */
export function inOutExpo(t: number) {
    if (0 === t) return 0;
    if (1 === t) return 1;
    if ((t *= 2) < 1) return .5 * Math.pow(1024, t - 1);
    return .5 * (-Math.pow(2, -10 * (t - 1)) + 2);
}

/**
 * @name inCirc
 * @desc inCirc
 * @param {number} t
 * @returns {number}
 */
export function inCirc(t: number) {
    return 1 - Math.sqrt(1 - t * t);
}

/**
 * @name outCirc
 * @desc outCirc
 * @param {number} t
 * @returns {number}
 */
export function outCirc(t: number) {
    return Math.sqrt(1 - (--t * t));
}

/**
 * @name inOutCirc
 * @desc inOutCirc
 * @param {number} t
 * @returns {number}
 */
export function inOutCirc(t: number) {
    t *= 2;
    if (t < 1) return -0.5 * (Math.sqrt(1 - t * t) - 1);
    return 0.5 * (Math.sqrt(1 - (t -= 2) * t) + 1);
}

/**
 * @name inBack
 * @desc inBack
 * @param {number} t
 * @returns {number}
 */
export function inBack(t: number) {
    let s = 1.70158;
    return t * t * ((s + 1) * t - s);
}

/**
 * @name outBack
 * @desc outBack
 * @param {number} t
 * @returns {number}
 */
export function outBack(t: number) {
    let s = 1.70158;
    return --t * t * ((s + 1) * t + s) + 1;
}

/**
 * @name inOutBack
 * @desc inOutBack
 * @param {number} t
 * @returns {number}
 */
export function inOutBack(t: number) {
    let s = 1.70158 * 1.525;
    if ((t *= 2) < 1) return 0.5 * (t * t * ((s + 1) * t - s));
    return 0.5 * ((t -= 2) * t * ((s + 1) * t + s) + 2);
}

/**
 * @name inBounce
 * @desc inBounce
 * @param {number} t
 * @returns {number}
 */
export function inBounce(t: number) {
    return 1 - outBounce(1 - t);
}

/**
 * @name outBounce
 * @desc outBounce
 * @param {number} t
 * @returns {number}
 */
export function outBounce(t: number) {
    if (t < (1 / 2.75)) {
        return 7.5625 * t * t;
    } else if (t < (2 / 2.75)) {
        return 7.5625 * (t -= (1.5 / 2.75)) * t + 0.75;
    } else if (t < (2.5 / 2.75)) {
        return 7.5625 * (t -= (2.25 / 2.75)) * t + 0.9375;
    } else {
        return 7.5625 * (t -= (2.625 / 2.75)) * t + 0.984375;
    }
}

/**
 * @name inOutBounce
 * @desc inOutBounce
 * @param {number} t
 * @returns {number}
 */
export function inOutBounce(t: number) {
    if (t < .5) return inBounce(t * 2) * .5;
    return outBounce((t * 2 - 1) * .5 + .5);
}

/**
 * @name inElastic
 * @desc inElastic
 * @param {number} t
 * @returns {number}
 */
export function inElastic(t: number) {
    let s, a = 0.1, p = 0.4;
    if (t === 0) return 0;
    if (t === 1) return 1;
    if (!a || a < 1) {
        a = 1;
        s = p / 4;
    } else {
        s = p * Math.asin(1 / a) / (2 * Math.PI);
    }
    return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - s) * (2 * Math.PI) / p));
}

/**
 * @name outElastic
 * @desc outElastic
 * @param {number} t
 * @returns {number}
 */
export function outElastic(t: number) {
    let s, a = 0.1, p = 0.4;
    if (t === 0) return 0;
    if (t === 1) return 1;
    if (!a || a < 1) {
        a = 1;
        s = p / 4;
    } else {
        s = p * Math.asin(1 / a) / (2 * Math.PI);
    }
    return (a * Math.pow(2, -10 * t) * Math.sin((t - s) * (2 * Math.PI) / p) + 1);
}

/**
 * @name inOutElastic
 * @desc inOutElastic
 * @param {number} t
 * @returns {number}
 */
export function inOutElastic(t: number) {
    let s, a = 0.1, p = 0.4;
    if (t === 0) return 0;
    if (t === 1) return 1;
    if (!a || a < 1) {
        a = 1;
        s = p / 4;
    } else {
        s = p * Math.asin(1 / a) / (2 * Math.PI);
    }
    if ((t *= 2) < 1) {
        return -0.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - s) * (2 * Math.PI) / p));
    }
    return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t - s) * (2 * Math.PI) / p) * 0.5 + 1;
}