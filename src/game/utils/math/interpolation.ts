/**
 * @name lerp
 * @desc lerp
 * @param {number} a
 * @param {number} b
 * @param {number} t
 * @returns {number}
 */
export function lerp(a: number, b: number, t: number): number {
    return a + (b - a) * t;
}