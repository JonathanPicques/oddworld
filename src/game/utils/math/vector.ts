import {Vector2 as ThreeVector2} from "three";

/**
 * @name Vector2
 */
export class Vector2 extends ThreeVector2 {}