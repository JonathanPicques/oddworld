import spine from "../../vendors/spine/spine-threejs";

import {ICoroutine} from "./coroutine/coroutine";

import {yieldForPromise} from "./coroutine/yield";

/**
 * @name AssetLoader
 * @desc responsible for lazy/cached loading of business assets served over the network
 */
export class AssetLoader {

    /**
     * @name Assets
     * @desc generated map of available assets
     */
    public static readonly Assets: { "graphics.mudokon": AssetEntrySpine } = {
        "graphics.mudokon": {
            "png": "assets/graphics/mudokon/mudokon.png",
            "json": "assets/graphics/mudokon/mudokon.json",
            "atlas": "assets/graphics/mudokon/mudokon.atlas"
        }
    };

    /**
     * @name cache
     * @desc cached assets previously loaded
     */
    private readonly cache: { readonly "spine": Map<AssetEntrySpine, spine.SkeletonData> };

    /**
     * @name constructor
     */
    public constructor() {
        this.cache = {"spine": new Map()};
    }

    /**
     * @name loadSpine
     * @desc load a spine skeleton
     * @param {AssetEntrySpine} assetEntry
     * @returns {ICoroutine}
     */
    public * loadSpine(assetEntry: AssetEntrySpine): ICoroutine {
        if (this.cache.spine.has(assetEntry)) {
            return this.cache.spine.get(assetEntry);
        }
        yield `Loading ${assetEntry.png}`;
        const spinePng = yield* yieldForPromise(AssetLoader.loadBlob(assetEntry.png));
        yield `Loading ${assetEntry.json}`;
        const spineJson = yield* yieldForPromise(AssetLoader.loadJson(assetEntry.json));
        yield `Loading ${assetEntry.atlas}`;
        const spineAtlas = yield* yieldForPromise(AssetLoader.loadText(assetEntry.atlas));
        const image = new Image();
        const imageLoad = new Promise(resolve => {
            image.onload = resolve;
        });
        image.src = window.URL.createObjectURL(spinePng);
        yield *yieldForPromise(imageLoad);
        const atlas = new spine.TextureAtlas(spineAtlas, () => {
            return new spine.threejs.ThreeJsTexture(image);
        });
        const atlasLoader = new spine.AtlasAttachmentLoader(atlas);
        const skeletonJson = new spine.SkeletonJson(atlasLoader);
        const skeletonData = skeletonJson.readSkeletonData(spineJson);
        this.cache.spine.set(assetEntry, skeletonData);
        return skeletonData;
    }

    /**
     * @name getSpine
     * @desc return a cached spine asset or null
     * @param {AssetEntrySpine} assetEntry
     * @returns {spine.SkeletonData}
     */
    public getSpine(assetEntry: AssetEntrySpine): spine.SkeletonData {
        return this.cache.spine.get(assetEntry) || null;
    }

    /**
     * @name loadBlob
     * @desc async load a blob from the network
     * @param {string} path
     * @returns {Promise<Blob>}
     */
    private static async loadBlob(path: string): Promise<Blob> {
        return await (await fetch(path)).blob();
    }

    /**
     * @name loadJson
     * @desc async load a json from the network
     * @param {string} path
     * @returns {Promise<object>}
     */
    private static async loadJson(path: string): Promise<object> {
        return await (await fetch(path)).json();
    }

    /**
     * @name loadText
     * @desc async load a text from the network
     * @param {string} path
     * @returns {Promise<Blob>}
     */
    private static async loadText(path: string): Promise<string> {
        return await (await fetch(path)).text();
    }

}

/**
 * @name AssetEntrySpine
 * @desc entry for a spine skeleton asset (skeleton data, atlas and texture)
 */
export interface AssetEntrySpine {
    png: string;
    json: string;
    atlas: string;
}