import {Entity} from "../entity";
import {IController} from "../../controllers/controller";
import {MudokonEntity} from "../alive/mudokon/mudokon";
import {InputKeyboardKey} from "../../utils/input/keyboard";

export class PlayerController extends Entity implements IController {

    /**
     * @name pawn
     * @desc controlled pawn
     */
    private pawn: MudokonEntity;

    /**
     * @name possess
     * @desc {@link IController.possess}
     * @param {IPawn} pawn
     */
    public possess(pawn: MudokonEntity): void {
        this.pawn = pawn;
        this.pawn.controller = this;
    }

    /**
     * @name unpossess
     * @desc {@link IController.unpossess}
     */
    public unpossess(): void {
        this.pawn.controller = null;
        this.pawn = null;
    }

    /**
     * @name update
     * @desc {@link Entity.update}
     */
    public update(): void {
        if (this.pawn !== null) {
            this.pawn.input.up = this.level.game.inputManager.keyboardKeyDown(InputKeyboardKey.UP_ARROW);
            this.pawn.input.run = this.level.game.inputManager.keyboardKeyDown(InputKeyboardKey.SHIFT);
            this.pawn.input.jump = this.level.game.inputManager.keyboardKeyDown(InputKeyboardKey.SPACE);
            this.pawn.input.down = this.level.game.inputManager.keyboardKeyDown(InputKeyboardKey.DOWN_ARROW);
            this.pawn.input.left = this.level.game.inputManager.keyboardKeyDown(InputKeyboardKey.LEFT_ARROW);
            this.pawn.input.right = this.level.game.inputManager.keyboardKeyDown(InputKeyboardKey.RIGHT_ARROW);
            this.pawn.input.chant = this.level.game.inputManager.keyboardKeyDown(InputKeyboardKey.N0);
            this.pawn.input.crouch = this.level.game.inputManager.keyboardKeyDown(InputKeyboardKey.CTRL);
        }
    }

}