import {Vector2} from "../../utils/math/vector";

/**
 * @name IAlive
 */
export interface IAlive {

    /**
     * @name position
     * @desc position of the alive entity
     */
    position: Vector2;

    /**
     * @name direction
     * @desc faced direction of the alive entity
     */
    direction: AliveDirection;

}

/**
 * @name AliveDirection
 */
export enum AliveDirection {

    /**
     * @name Up
     */
    Up = 1,

    /**
     * @name Down
     */
    Down = -1,

    /**
     * @name Left
     */
    Left = -1,

    /**
     * @name Right
     */
    Right = 1,

}