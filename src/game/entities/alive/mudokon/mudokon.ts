import spine from "../../../../vendors/spine/spine-threejs";

import {IPawn} from "../../../controllers/pawn";
import {Entity} from "../../entity";
import {Vector2} from "../../../utils/math/vector";
import {ICoroutine} from "../../../utils/coroutine/coroutine";
import {AssetLoader} from "../../../utils/loader";
import {IController} from "../../../controllers/controller";
import {AliveDirection, IAlive} from "../alive";

import {lerp} from "../../../utils/math/interpolation";
import {yieldForScaledSeconds} from "../../../utils/coroutine/yield";
import {TEasing, inQuad, linear, outQuad, outBack, inBack} from "../../../utils/math/easing";

const gridSize = 35;
const runDuration = 0.21;
const walkDuration = 0.4;

/**
 * @name MudokonEntity
 */
export class MudokonEntity extends Entity implements IAlive, IPawn {

    /**
     * @name position
     * @desc {@link IAlive.position}
     */
    public position: Vector2;
    /**
     * @name direction
     * @desc {@link IAlive.direction}
     */
    public direction: AliveDirection;
    /**
     * @name controller
     * @desc {@link IPawn.controller}
     */
    public controller: IController;

    /**
     * @name input
     * @desc input applied to this mudokon
     */
    public input: TMudokonInput<boolean>;
    /**
     * @name inputFrames
     * @desc frame data of the inputs
     */
    private inputFrames: TMudokonInput<number>;
    /**
     * @name mudokonMesh
     * @desc mudokon skeleton mesh
     */
    private mudokonMesh: spine.threejs.SkeletonMesh;
    /**
     * @name gameTimeScale
     * @desc game time scale
     */
    private gameTimeScale: () => number;

    /**
     * @name start
     * @desc called when the entity is added to the level
     */
    public start(): void {
        this.input = {
            "up": false,
            "run": false,
            "jump": false,
            "down": false,
            "left": false,
            "right": false,
            "chant": false,
            "crouch": false
        };
        this.inputFrames = {
            "up": 0,
            "run": 0,
            "jump": 0,
            "down": 0,
            "left": 0,
            "right": 0,
            "chant": 0,
            "crouch": 0
        };
        this.position = new Vector2(); // TODO: would be filled by map
        this.direction = AliveDirection.Right; // TODO: would be filled by map
        this.mudokonMesh = new spine.threejs.SkeletonMesh(this.level.game.assetLoader.getSpine(AssetLoader.Assets["graphics.mudokon"]));
        this.gameTimeScale = () => this.level.game.deltaTime;

        this.mudokonMesh.state.setAnimation(0, "idle", true);
        this.mudokonMesh.state.data.defaultMix = 0.30;
        this.level.scene.add(this.mudokonMesh);
        this.level.game.coroutineRunner.startCoroutine(this.idleState());
    }

    /**
     * @name update
     * @desc called when the level/entity is updated
     */
    public update(): void {
        for (const input in this.input) {
            if (!this.input[input]) {
                this.inputFrames[input] = 0;
            }
            if (this.input[input]) {
                this.inputFrames[input] += 1;
            }
        }
        this.mudokonMesh.update(this.level.game.deltaTime);
        this.mudokonMesh.position.set(this.position.x, this.position.y, 0);
    }

    /**
     * @name LoadAssets
     * @returns {ICoroutine}
     */
    public static * LoadAssets(assetLoader: AssetLoader): ICoroutine {
        yield* assetLoader.loadSpine(AssetLoader.Assets["graphics.mudokon"]);
    }

    /**
     * @name wasRunning
     * @desc whether the mudokon was running in the previous state
     */
    private wasRunning: boolean = false;

    /**
     * @name idleState
     * @desc idle movement
     * @returns {ICoroutine}
     */
    private * idleState(): ICoroutine {
        console.log("idleState");
        while (true) {
            const wasRunning = this.wasRunning;
            this.wasRunning = false;
            switch (true) {
                case this.input.left:
                    if (wasRunning && this.direction !== AliveDirection.Left) {
                        yield* this.runChangeDirectionState(AliveDirection.Left);
                        this.wasRunning = true;
                    }
                    if (this.direction !== AliveDirection.Left) yield* this.idleChangeDirectionState(AliveDirection.Left);
                    if (this.input.run && this.input.left) {
                        yield* this.runState(AliveDirection.Left);
                        this.wasRunning = true;
                    }
                    if (!this.input.run && this.input.left) yield* this.walkState(AliveDirection.Left);
                    break;
                case this.input.right:
                    if (wasRunning && this.direction !== AliveDirection.Right) {
                        yield* this.runChangeDirectionState(AliveDirection.Right);
                        this.wasRunning = true;
                    }
                    if (this.direction !== AliveDirection.Right) yield* this.idleChangeDirectionState(AliveDirection.Right);
                    if (this.input.run && this.input.right) {
                        yield* this.runState(AliveDirection.Right);
                        this.wasRunning = true;
                    }
                    if (!this.input.run && this.input.right) yield* this.walkState(AliveDirection.Right);
                    break;
                case this.input.chant: {
                    yield* this.idleChantState();
                    break;
                }
                case this.input.up: {
                    yield* this.hopInPlaceState();
                    break;
                }
                case this.input.jump: {
                    yield *this.moveGridJump(this.direction, 3, 1, walkDuration);
                    break;
                }
            }
            yield;
        }
    }

    /**
     * @name idleChangeDirectionState
     * @desc change the direction while idle
     * @param {AliveDirection} direction
     * @returns {ICoroutine}
     */
    private * idleChangeDirectionState(direction: AliveDirection): ICoroutine {
        console.log("idleChangeDirectionState");
        for (const [progress] of this.moveGridHorizontal(-direction, 1, walkDuration)) {
            this.mudokonMesh.scale.x = lerp(-direction, direction, progress);
            yield;
        }
        this.direction = direction;
        this.mudokonMesh.scale.x = direction;
    }

    /**
     * @name runState
     * @desc run in the given direction
     * @param {AliveDirection} direction
     * @returns {ICoroutine}
     */
    private * runState(direction: AliveDirection): ICoroutine {
        console.log("runState");
        while (this.input.run && this.inputHeldInDirection(direction)) {
            yield* this.moveGridHorizontal(direction, 3, runDuration * 3);
        }
        if (this.input.run && this.inputHeldInOtherDirection(direction)) {
            yield* this.runChangeDirectionState(direction === AliveDirection.Left ? AliveDirection.Right : AliveDirection.Left);
        } else {
            yield* this.runToIdleState(direction);
        }
    }

    /**
     * @name idleChangeDirectionState
     * @desc change the direction while running
     * @param {AliveDirection} direction
     * @returns {ICoroutine}
     */
    private * runChangeDirectionState(direction: AliveDirection): ICoroutine {
        console.log("runChangeDirectionState");
        for (const [progress] of this.moveGridHorizontal(-direction, 2, runDuration, outQuad)) {
            this.mudokonMesh.scale.x = lerp(-direction, direction, progress);
            yield;
        }
        this.direction = direction;
        this.mudokonMesh.scale.x = direction;
    }

    /**
     * @name idleChangeDirectionState
     * @desc transition to idle from running
     * @param {AliveDirection} direction
     * @returns {ICoroutine}
     */
    private * runToIdleState(direction: AliveDirection): ICoroutine {
        console.log("runToIdleState");
        yield* this.moveGridHorizontal(direction, 2, runDuration * 2);
    }

    /**
     * @name runState
     * @desc walk in the given direction
     * @param {AliveDirection} direction
     * @returns {ICoroutine}
     */
    private * walkState(direction: AliveDirection): ICoroutine {
        console.log("walkState");
        while (!this.input.run && this.inputHeldInDirection(direction)) {
            yield* this.moveGridHorizontal(direction, 1, walkDuration);
        }
    }

    /**
     * @name idleChantState
     * @desc chant while pressing button
     * @returns {ICoroutine}
     */
    private * idleChantState(): ICoroutine {
        console.log("chantState");
        this.mudokonMesh.state.setAnimation(0, "chanting", true);
        while (this.input.chant) {
            yield* yieldForScaledSeconds(0.5, this.gameTimeScale);
        }
        this.mudokonMesh.state.setAnimation(0, "idle", true);
        yield* yieldForScaledSeconds(0.1, this.gameTimeScale);
    }

    /**
     * @name hopInPlaceState
     * @desc hop in place
     * @returns {ICoroutine}
     */
    private * hopInPlaceState(): ICoroutine {
        this.mudokonMesh.state.setAnimation(0, "hop", false);
        yield* yieldForScaledSeconds(0.3, this.gameTimeScale);
        yield* this.moveGridVertical(AliveDirection.Up, 2, walkDuration, outQuad);
        yield* this.moveGridVertical(AliveDirection.Down, 2, walkDuration, inQuad);
        yield* yieldForScaledSeconds(0.18, this.gameTimeScale);
        this.mudokonMesh.state.setAnimation(0, "idle", true);
    }

    /**
     * @name moveGridHorizontal
     * @desc move from the given cells number on the grid in the given direction and yield the progress
     * @param {AliveDirection} direction
     * @param {number} cells
     * @param {number} duration
     * @param {Function} easing
     * @returns {ICoroutine<number>}
     */
    private * moveGridHorizontal(direction: AliveDirection, cells: number, duration: number, easing: TEasing = linear): ICoroutine<[number, number]> {
        const origin = this.position.x;
        const position = this.position.x + direction * gridSize * cells;
        for (let timeElapsed = 0; timeElapsed < duration; timeElapsed += this.level.game.deltaTime) {
            const deltaTime = timeElapsed / duration;
            const easeDeltaTime = easing(deltaTime);
            this.position.x = lerp(origin, position, easeDeltaTime);
            yield [deltaTime, easeDeltaTime];
        }
        this.position.x = position;
    }

    /**
     * @name moveGridHorizontal
     * @desc move from the given cells number on the grid in the given direction and yield the progress
     * @param {AliveDirection} direction
     * @param {number} cells
     * @param {number} duration
     * @param {Function} easing
     * @returns {ICoroutine<number>}
     */
    private * moveGridVertical(direction: AliveDirection, cells: number, duration: number, easing: TEasing = linear): ICoroutine<[number, number]> {
        const origin = this.position.y;
        const position = this.position.y + direction * gridSize * cells;
        for (let timeElapsed = 0; timeElapsed < duration; timeElapsed += this.level.game.deltaTime) {
            const deltaTime = timeElapsed / duration;
            const easeDeltaTime = easing(deltaTime);
            this.position.y = lerp(origin, position, easeDeltaTime);
            yield [deltaTime, easeDeltaTime];
        }
        this.position.y = position;
    }

    /**
     * @name moveGridJump
     * @desc make a jump
     * @param {AliveDirection} direction
     * @param {number} cells
     * @param {number} height
     * @param {number} duration
     * @returns {ICoroutine<number>}
     */
    private * moveGridJump(direction: AliveDirection, cells: number, height: number, duration: number) : ICoroutine {
        const origin = this.position.clone();
        const position = this.position.x + direction * gridSize * cells;
        for (let timeElapsed = 0; timeElapsed < duration; timeElapsed += this.level.game.deltaTime) {
            this.position.x = lerp(origin.x, position, timeElapsed / duration);
            if (timeElapsed < duration / 2) {
                this.position.y = lerp(origin.y, origin.y + gridSize * height, outBack(timeElapsed / duration));
            } else {
                this.position.y = lerp(origin.y + gridSize * height, origin.y, inBack(timeElapsed / duration));
            }
            yield 0;
        }
        this.position.set(position, origin.y);
    }

    /**
     * @name inputAndDirectionMatch
     * @desc return true when the input direction held is the same as the given mudokon
     * @param {AliveDirection} direction
     * @returns {boolean}
     */
    private inputHeldInDirection(direction: AliveDirection) {
        return (direction === AliveDirection.Left && this.input.left && !this.input.right) ||
            (direction === AliveDirection.Right && !this.input.left && this.input.right);
    }

    /**
     * @name inputAndDirectionMatch
     * @desc return true when the input direction held is the same as the given mudokon
     * @param {AliveDirection} direction
     * @returns {boolean}
     */
    private inputHeldInOtherDirection(direction: AliveDirection) {
        return (direction === AliveDirection.Left && !this.input.left && this.input.right) ||
            (direction === AliveDirection.Right && this.input.left && !this.input.right);
    }

}

/**
 * @name TMudokonInput
 */
type TMudokonInput<T> = {
    [key: string]: T,
    "up": T
    "run": T
    "jump": T
    "down": T
    "left": T
    "right": T
    "chant": T
    "crouch": T
};