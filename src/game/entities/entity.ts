import {Level} from "../levels/level";

export abstract class Entity {

    /**
     * @name game
     * @desc reference to the game
     */
    public readonly level: Level;

    /**
     * @name constructor
     * @param {Level} level
     */
    public constructor(level: Level) {
        this.level = level;
    }

    /**
     * @name start
     * @desc called when the entity is added to the level
     */
    public start(): void {

    }

    /**
     * @name update
     * @desc called when the level/entity is updated
     */
    public update(): void {

    }

    /**
     * @name stop
     * @desc called when the entity is removed from the level
     */
    public stop(): void {

    }

}