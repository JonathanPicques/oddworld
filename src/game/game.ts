import {PerspectiveCamera, Vector2, WebGLRenderer} from "three";

import {Level} from "./levels/level";
import {AssetLoader} from "./utils/loader";
import {ICoroutine} from "./utils/coroutine/coroutine";
import {InputManager} from "./utils/input/input";
import {CoroutineRunner} from "./utils/coroutine/runner";

/**
 * @name Game
 */
export class Game {

    /**
     * @name size
     * @desc size of the window
     */
    public readonly size: Vector2;
    /**
     * @name camera
     * @desc camera used to render the level
     */
    public readonly camera: PerspectiveCamera;
    /**
     * @name renderer
     * @desc webgl renderer
     */
    public readonly renderer: WebGLRenderer;
    /**
     * @name assetLoader
     * @desc asset loader with cache
     */
    public readonly assetLoader: AssetLoader;
    /**
     * @name inputManager
     * @desc handle player input (keyboard, mouse and gamepads)
     */
    public readonly inputManager: InputManager;
    /**
     * @name coroutines
     * @desc coroutines runner
     */
    public readonly coroutineRunner: CoroutineRunner;

    /**
     * @name level
     * @desc current level updated by the game loop
     */
    public level: Level;
    /**
     * @name running
     * @desc whether the tick loop is running
     */
    public running: boolean;
    /**
     * @name deltaTime
     * @desc time in seconds it took to complete the last frame
     */
    public deltaTime: number;
    /**
     * @name elapsedTime
     * @desc time in seconds for how long the tick loop was running
     */
    public elapsedTime: number;
    /**
     * @name changeLevelPromise
     * @desc promise when changeLevel is in progress
     */
    private changeLevelPromise: Promise<void>;

    /**
     * @name constructor
     */
    public constructor() {
        this.size = new Vector2(window.innerWidth, window.innerHeight);
        this.camera = new PerspectiveCamera(75, this.size.x / this.size.y, 1, 3000);
        this.renderer = new WebGLRenderer();
        this.assetLoader = new AssetLoader();
        this.inputManager = new InputManager(this.renderer.domElement);
        this.coroutineRunner = new CoroutineRunner();

        this.level = null;
        this.running = false;
        this.deltaTime = 0.0;
        this.elapsedTime = 0.0;
        this.changeLevelPromise = Promise.resolve();

        this.initializeThreeJS();
    }

    /**
     * @name initializeThreeJS
     * @desc initialize the threejs components
     */
    private initializeThreeJS(): void {
        this.camera.position.set(0, 100, 300);
        this.renderer.setClearColor(0x334455);
        this.renderer.setSize(this.size.x, this.size.y);
        window.addEventListener("resize", () => {
            this.size.set(window.innerWidth, window.innerHeight);
            this.renderer.setSize(this.size.x, this.size.y);
            this.camera.aspect = window.innerWidth / window.innerHeight;
            this.camera.updateProjectionMatrix();
        });
        window.addEventListener("focus", () => {
           this.renderer.domElement.focus();
        });
        document.body.appendChild(this.renderer.domElement);
        this.renderer.domElement.focus();
    }

    /**
     * @name start
     * @desc start the infinite game loop
     */
    public start(): void {
        if (!this.running) {
            this.running = true;
            let lastTimestamp = 0;
            const updateLoop = (timestamp: number) => {
                if (lastTimestamp === 0) {
                    lastTimestamp = timestamp;
                }
                this.deltaTime = (timestamp - lastTimestamp) / 1000.0;
                this.elapsedTime += this.deltaTime;
                lastTimestamp = timestamp;
                this.coroutineRunner.runCoroutines();
                if (this.level !== null) {
                    this.level.update();
                    this.renderer.render(this.level.scene, this.camera);
                }
                if (this.running) {
                    requestAnimationFrame(updateLoop);
                }
            };
            requestAnimationFrame(updateLoop);
        }
    }

    /**
     * @name stop
     * @desc stop the infinite game loop on the next frame
     */
    public stop(): void {
        this.running = false;
    }

    /**
     * @name changeLevel
     * @desc change the current level by stopping the previous one and starting the given one
     * @returns {Promise<void>}
     */
    public changeLevel(level: Level): Promise<void> {
        if (!this.running) {
            throw new Error("Game::changeLevel() cannot be called when the game is not started");
        }
        const game = this;
        const coroutine = (function* (): ICoroutine {
            if (game.level !== null) {
                yield* game.level.stop();
                game.level = null;
            }
            yield* level.start();
            game.level = level;
        })();
        this.changeLevelPromise = <Promise<any>>this.changeLevelPromise
            .then(() => new Promise(resolve => {
                coroutine.end = () => resolve();
                coroutine.update = (coroutine: ICoroutine) => {
                    if (coroutine.value && typeof coroutine.value === "string") {
                        console.log(coroutine.value);
                    }
                };
                this.coroutineRunner.startCoroutine(coroutine);
            }));
        return this.changeLevelPromise;
    }

}