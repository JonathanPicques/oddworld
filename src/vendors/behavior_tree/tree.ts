import {BehaviorTreeNode} from "./core/node";
import {BehaviorTreeTicker} from "./core/ticker";

/**
 * @name BehaviorTree
 */
export class BehaviorTree<T> {

    /**
     * @name agent
     * @desc agent controlled by this behavior tree
     */
    private readonly agent: T;

    /**
     * @name root
     * @desc root node of the behavior tree
     */
    private readonly root: BehaviorTreeNode;
    /**
     * @name ticker
     * @desc responsible to make the tree tick
     */
    private readonly ticker: BehaviorTreeTicker<T>;

    /**
     * @name constructor
     * @param {BehaviorTreeNode} root
     * @param {T} agent
     */
    public constructor(agent: T, root: BehaviorTreeNode) {
        this.root = root;
        this.agent = agent;
        this.ticker = new BehaviorTreeTicker<T>(agent);
    }

    /**
     * @name tick
     * @desc start the tree ticking
     */
    public tick(): BehaviorTreeStatus {
        this.ticker.reset();
        return this.ticker.tick(this.root);
    }

}

/**
 * @name BehaviorTreeStatus
 */
export enum BehaviorTreeStatus {

    /**
     * @name Running
     * @desc node is currently running and may return success or failure later
     */
    Running,

    /**
     * @name Success
     * @desc node exited with status success
     */
    Success,

    /**
     * @name Failure
     * @desc node exited with status failure
     */
    Failure

}