import {BehaviorTreeNode} from "./node";
import {BehaviorTreeStatus} from "../tree";

/**
 * @name BehaviorTreeTicker
 */
export class BehaviorTreeTicker<T> {

    /**
     * @name agent
     * @desc agent controlled by this behavior tree
     */
    public readonly agent: T;

    /**
     * @name visited
     * @desc array of visited nodes
     */
    private visited: Array<BehaviorTreeNode>;

    /**
     * @name constructor
     * @param {T} agent
     */
    constructor(agent: T) {
        this.agent = agent;
    }

    /**
     * @name tick
     * @desc make the node tick
     * @param {BehaviorTreeNode} node
     */
    tick(node: BehaviorTreeNode): BehaviorTreeStatus {
        if (this.visited.indexOf(node) === -1) {
            this.visited.push(node);
            node.start(this);
        }
        node.enter(this);
        const status = node.tick(this);
        node.exit(this);
        if (status !== BehaviorTreeStatus.Running) {
            this.visited.splice(this.visited.indexOf(node), 1);
            node.stop(this);
        }
        return status;
    }

    /**
     * @name reset
     * @desc reset the ticker state for next tick
     */
    reset() {
        this.visited = [];
    }

}