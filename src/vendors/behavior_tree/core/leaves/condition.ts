import {BehaviorTreeLeaf} from "./leaf";
import {BehaviorTreeTicker} from "../ticker";
import {BehaviorTreeStatus} from "../../tree";

export class BehaviorTreeConditionLeaf extends BehaviorTreeLeaf {

    /**
     * @name condition
     * @desc condition function to execute each tick that returns success (true) or failure (false)
     */
    private readonly condition: () => BehaviorTreeStatus.Success | BehaviorTreeStatus.Failure;

    /**
     * @name constructor
     * @param {() => BehaviorTreeStatus} condition
     */
    constructor(condition: () => BehaviorTreeStatus.Success | BehaviorTreeStatus.Failure) {
        super();
        this.condition = condition;
    }

    /**
     * @name tick
     * @desc {@link BehaviorTreeLeaf.tick}
     * @param {BehaviorTreeTicker<T>} ticker
     * @returns {BehaviorTreeStatus}
     */
    public tick<T>(ticker: BehaviorTreeTicker<T>) : BehaviorTreeStatus {
        return this.condition();
    }

}