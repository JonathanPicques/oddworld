import {BehaviorTreeLeaf} from "./leaf";
import {BehaviorTreeTicker} from "../ticker";
import {BehaviorTreeStatus} from "../../tree";

export class BehaviorTreeActionLeaf extends BehaviorTreeLeaf {

    /**
     * @name action
     * @desc action function to execute each tick
     */
    private readonly action: () => BehaviorTreeStatus;

    /**
     * @name constructor
     * @param {() => BehaviorTreeStatus} action
     */
    constructor(action: () => BehaviorTreeStatus) {
        super();
        this.action = action;
    }

    /**
     * @name tick
     * @desc {@link BehaviorTreeLeaf.tick}
     * @param {BehaviorTreeTicker<T>} ticker
     * @returns {BehaviorTreeStatus}
     */
    public tick<T>(ticker: BehaviorTreeTicker<T>) : BehaviorTreeStatus {
        return this.action();
    }

}