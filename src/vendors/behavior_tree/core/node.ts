import {BehaviorTreeTicker} from "./ticker";
import {BehaviorTreeStatus} from "../tree";

/**
 * @name BehaviorTreeNode
 */
export abstract class BehaviorTreeNode {

    /**
     * @name start
     * @desc called when the node is started (not yet ticked or previously stopped)
     * @param {BehaviorTreeTicker} ticker
     */
    public start<T>(ticker: BehaviorTreeTicker<T>): void {

    }

    /**
     * @name enter
     * @desc called when the node is entered each tick
     * @param {BehaviorTreeTicker} ticker
     */
    public enter<T>(ticker: BehaviorTreeTicker<T>): void {

    }

    /**
     * @name tick
     * @desc called on each tick
     * @param {BehaviorTreeTicker} ticker
     * @returns {BehaviorTreeStatus}
     */
    public abstract tick<T>(ticker: BehaviorTreeTicker<T>): BehaviorTreeStatus;

    /**
     * @name exit
     * @desc called when the node is exited each tick
     * @param {BehaviorTreeTicker} ticker
     */
    public exit<T>(ticker: BehaviorTreeTicker<T>): void {

    }

    /**
     * @name stop
     * @desc called when the node is stopped (tick return status is success or failure)
     * @param {BehaviorTreeTicker} ticker
     */
    public stop<T>(ticker: BehaviorTreeTicker<T>): void {

    }

}