import {BehaviorTreeNode} from "../node";

/**
 * @name BehaviorTreeDecorator
 */
export abstract class BehaviorTreeDecorator extends BehaviorTreeNode {

    /**
     * @name child
     * @desc decorated child
     */
    protected readonly child: BehaviorTreeNode;

    /**
     * @name constructor
     * @param {BehaviorTreeNode} child
     */
    public constructor(child: BehaviorTreeNode) {
        super();
        this.child = child;
    }

}