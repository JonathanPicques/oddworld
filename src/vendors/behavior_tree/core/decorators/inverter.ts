import {BehaviorTreeTicker} from "../ticker";
import {BehaviorTreeStatus} from "../../tree";
import {BehaviorTreeDecorator} from "./decorator";

export class BehaviorTreeInverterDecorator extends BehaviorTreeDecorator {

    /**
     * @name tick
     * @desc {@link BehaviorTreeDecorator.tick}
     * @param {BehaviorTreeTicker<T>} ticker
     * @returns {BehaviorTreeStatus}
     */
    public tick<T>(ticker: BehaviorTreeTicker<T>): BehaviorTreeStatus {
        switch (ticker.tick(this.child)) {
            case BehaviorTreeStatus.Success:
                return BehaviorTreeStatus.Failure;
            case BehaviorTreeStatus.Failure:
                return BehaviorTreeStatus.Success;
            case BehaviorTreeStatus.Running:
                return BehaviorTreeStatus.Running;
            default:
                throw new Error();
        }
    }
}