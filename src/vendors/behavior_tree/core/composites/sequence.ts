import {BehaviorTreeTicker} from "../ticker";
import {BehaviorTreeStatus} from "../../tree";
import {BehaviorTreeComposite} from "./composite";

/**
 * @name BehaviorTreeSequence
 */
export class BehaviorTreeSequence extends BehaviorTreeComposite {

    /**
     * @name tick
     * @desc {@link BehaviorTreeComposite.tick}
     * @param {BehaviorTreeTicker<T>} ticker
     * @returns {BehaviorTreeStatus}
     */
    public tick<T>(ticker: BehaviorTreeTicker<T>): BehaviorTreeStatus {
        for (const child of this.children) {
            const status = ticker.tick(child);
            if (status !== BehaviorTreeStatus.Success) {
                return status;
            }
        }
        return BehaviorTreeStatus.Success;
    }

}