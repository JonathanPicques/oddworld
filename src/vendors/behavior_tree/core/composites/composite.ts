import {BehaviorTreeNode} from "../node";

/**
 * @name BehaviorTreeComposite
 */
export abstract class BehaviorTreeComposite extends BehaviorTreeNode {

    /**
     * @name children
     * @desc children nodes of this composite
     */
    protected readonly children: Array<BehaviorTreeNode>;

    /**
     * @name constructor
     * @param {Array<BehaviorTreeNode>} children
     */
    public constructor(children: Array<BehaviorTreeNode>) {
        super();
        this.children = children;
    }

}